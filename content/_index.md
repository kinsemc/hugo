## Overview
The Bucky Model is a SEIR model stratified by age and geographic location. Each county (or admin2) is split into 16 5-year age groups. For the US, this leads to a system of more than 48,000 coupled SEIR models.

To run the code, visit [our repo](https://github.com/mattkinsey/bucky).


[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Documentation Status](https://readthedocs.org/projects/docs/badge/?version=latest)](https://docs.bucky.link/en/latest/)

